import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in );
		Queue<Integer> Q1 = new LinkedList<Integer>();
		Queue<Integer> Q2 = new LinkedList<Integer>();
		List<Integer> result = new ArrayList<>();
		int n = in.nextInt();
		for (int i = 0; i < n; i++) {
			int num = in.nextInt();
			if(num%2 == 1)
				Q1.offer(num);
			if(num%2 == 0)
				Q2.offer(num);
		}
		while(!(Q1.isEmpty() && Q2.isEmpty())){
			if(Q1.isEmpty()){
				while(!Q2.isEmpty()){
					result.add(Q2.poll());
				}
			}
			if(Q2.isEmpty()){
				while(!Q1.isEmpty()){
					result.add(Q1.poll());
				}
			}
			if(!Q1.isEmpty() && !Q2.isEmpty()){
				if(Q1.size() == 1)
					result.add(Q1.poll());
				if(Q1.size() >= 2){
					result.add(Q1.poll());
					result.add(Q1.poll());
				}
				result.add(Q2.poll());
			}
		}
		for (int i = 0; i < result.size(); i++) {
			if( i == result.size()-1)
				System.out.print(result.get(i));
			else
				System.out.print(result.get(i)+" ");
		}
	}
}