package One;

import java.util.ArrayList;

class Fruit{
	private String name;
	
	public Fruit(String name){
		this.name = name;
	}

	public String toString() {
		return super.toString() + "-" + name;
	}

	/*@Override
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Fruit other = (Fruit) obj;
	    if (name == null) {
	        if (other.name != null)
	            return false;
	    } else if (!name.equalsIgnoreCase(other.name))
	        return false;
	    return true;
	}*/
}

public class ManagerTest
{
   public static void main(String[] args)
   {
	  ArrayList<Fruit> fruitList = new ArrayList<Fruit>();
	  Fruit[] f = new Fruit[6];
      f[0] = new Fruit("apples");
      f[1] = new Fruit("Apples");
      f[2] = new Fruit("bananas");
      f[3] = new Fruit("baNanaS");
      f[4] = new Fruit("melon");
      f[5] = new Fruit("MeLon");
      for(int i = 0;i < 6;i++){
    	  if (fruitList.contains(f[i]) == false)
              fruitList.add(f[i]);
      }
          System.out.println(fruitList);
   }
}
