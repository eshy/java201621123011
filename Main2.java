package Main;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class Commodity{
	String name;
	int price;
	static ArrayList<Commodity> commodity = new ArrayList();
	
	public Commodity(String name,int price){
		this.name = name;
		this.price = price;
		commodity.add(this);
	}
	
}

class ShoppingCart{
	String name;
	int price;
	int num;
	static ArrayList<ShoppingCart> shoppingcart = new ArrayList();
	
	public ShoppingCart(String name,int price,int num){
		this.name = name;
		this.price = price;
		this.num = num;
		shoppingcart.add(this);
	}
}

public class Main2 {

	public static void main(String[] args) {
		int Price=0,Num,Sum=0;
		String Name;
		Commodity[] c = new Commodity[2];
		ShoppingCart[] s = new ShoppingCart[100];

		c[0] = new Commodity("keyboard",30);
		c[1] = new Commodity("mouse",20);
		
		JFrame jFrame = new JFrame("201621123011陈瑀");
		jFrame.setVisible(true);
		jFrame.setSize(300, 200);
		
		JButton jButton = new JButton("展示商品");
		JButton jButton1 = new JButton("购买商品");
		JButton jButton2 = new JButton("展示购物车");
		
		JPanel panel = new JPanel();
		panel.add(jButton);
		panel.add(jButton1);
		panel.add(jButton2);
		jFrame.add(panel,BorderLayout.CENTER);
		
		//展示商品
		jButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent arg) {
				// TODO 自动生成的方法存根
				String str = "";
				for(int i=0;i<c.length;i++){
					str += "商品名称:"+c[i].name+"   单价:"+c[i].price+"\n"; 
				}
				JOptionPane.showMessageDialog(null, "商品列表：\n"+str);
			}
		});
		
		//购买商品
		jButton1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg) {
				// TODO 自动生成的方法存根
				String Name = JOptionPane.showInputDialog("请输入要买的商品名称:");
				for(int i = 0;i < 2;i++){
					if(s[i]!=null&&Name.equals(s[i].name)){
						String str = JOptionPane.showInputDialog("请输入要再买的数量:");
						int Num = Integer.parseInt(str);
						s[i].num += Num;
						JOptionPane.showMessageDialog(null, "成功添加至购物车");
						break;
					}
					else if(Name.equals(c[i].name)){
						String str = JOptionPane.showInputDialog("请输入要买的数量:");
						int Num = Integer.parseInt(str);
						for(int flag = 0;flag<s.length;flag++){
							if(s[flag]==null){
								s[flag] = new ShoppingCart(Name,c[i].price,Num);
								break;
							}
						}
						JOptionPane.showMessageDialog(null, "成功添加至购物车");
						break;
					}
				}
			}
		});
		
		//展示购物车
		jButton2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO 自动生成的方法存根
				JFrame jFrame1 = new JFrame("201621123011陈瑀");
				jFrame1.setVisible(true);
				jFrame1.setSize(300, 200);
				String str = "";
				for(int i=0;s[i]!=null;i++){
					str += "   商品名称:"+s[i].name+"  单价:"+s[i].price+"  数量："+s[i].num; 
				}
				int n = 0;
				for(int i = 0;s[i]!=null;i++){
					n +=s[i].price*s[i].num;
				}
				final int n1 = n;
				JLabel jLabel = new JLabel(str);
				JLabel jLabel1 = new JLabel("购物车清单：\n");
				JLabel jLabel2 = new JLabel("总计："+n);
				JButton JB = new JButton("添加商品");
				JButton JB1 = new JButton("删除商品");
				JButton JB2 = new JButton("结算");
				JPanel JP = new JPanel();
				JPanel JP1 = new JPanel();
				JP.add(jLabel1);
				JP.add(jLabel);
				JP.add(jLabel2);
				JP1.add(JB);
				JP1.add(JB1);
				JP1.add(JB2);
				jFrame1.add(JP);
				jFrame1.add(JP1,BorderLayout.SOUTH);
				
				//添加商品
				JB.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO 自动生成的方法存根
						String Name = JOptionPane.showInputDialog("请输入添加的商品名称:");
						for(int i = 0;i < 2;i++){
							if(s[i]!=null&&Name.equals(s[i].name)){
								String str = JOptionPane.showInputDialog("请输入要再买的数量:");
								int Num = Integer.parseInt(str);
								s[i].num += Num;
								JOptionPane.showMessageDialog(null, "添加成功");
								break;
							}
							else if(Name.equals(c[i].name)){
								String str = JOptionPane.showInputDialog("请输入要买的数量:");
								int Num = Integer.parseInt(str);
								for(int flag = 0;flag<s.length;flag++){
									if(s[flag]==null){
										s[flag] = new ShoppingCart(Name,c[i].price,Num);
										break;
									}
								}
								JOptionPane.showMessageDialog(null, "添加成功");
								break;
							}
						}
					}
				});
				
				//删除商品
				JB1.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO 自动生成的方法存根
						String Name = JOptionPane.showInputDialog("请输入删除的商品名称:");
						for(int i = 0;i < 2;i++){
							if(s[i]!=null&&Name.equals(s[i].name)){
								String str = JOptionPane.showInputDialog("请输入要删除的数量:");
								int Num = Integer.parseInt(str);
								s[i].num -= Num;
								if(s[i].num<=0)
									s[i] = null;
								JOptionPane.showMessageDialog(null, "删除成功");
								break;
							}
						}
					}
				});
				
				//结算
				JB2.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO 自动生成的方法存根
						JOptionPane.showMessageDialog(null, "共付"+n1+"元\n谢谢惠顾！");
						}
				});
			}
		});
		jFrame.pack();
	}
}
